#-*- coding: utf-8 -*-
# Model and manager for multiautocomplete widget cache
# (c) 2011, Igor Dejanovic <igor DOT dejanovic AT gmail DOT com>
from django.db import models
from django.conf import settings
from utils import get_key

class MultiAutocompleteCacheManager(models.Manager):
    
    def update_cache(self, obj):
        fields = []
        # Fields for multiautocomplete widget must be specified in 
        # settings.MULTI_AUTOCOMPLETE_FIELDS
        for field in settings.MULTI_AUTOCOMPLETE_FIELDS:
            if obj._meta.app_label == field[0] and \
                obj._meta.module_name == field[1]:
                fields.append( (field[2], get_key(*field)))
                
        # If context is used, model should have get_context method
        # that returns integer which identifies context
        context = None
        if hasattr(obj, 'get_context'):
            context = obj.get_context()
            
        # Parser can be customized by a MULTI_AUTOCOMPLETE_PARSER setting
        parser_func = lambda in_str: [ x.strip() for x in in_str.splitlines()
                                        if x.strip()]
        if hasattr(settings, 'MULTI_AUTOCOMPLETE_PARSER'):
            parser_func = settings.MULTI_AUTOCOMPLETE_PARSER
        
        for field, key in fields:
            string_values = parser_func(getattr(obj, field))
            for string_value in string_values:
                try:
                    MultiAutocompleteCache.objects.get(key_hash=key, 
                        value=string_value)
                    MultiAutocompleteCache.objects.filter(key_hash=key, 
                        value=string_value).\
                        update(frequency=models.F('frequency')+1)
                except MultiAutocompleteCache.DoesNotExist:
                    item = MultiAutocompleteCache(key_hash=key, 
                        value=string_value,
                        context=context)
                    item.save()


class MultiAutocompleteCache(models.Model):
    
    key_hash = models.CharField(max_length=40, db_index=True)
    context = models.IntegerField(blank=True, null=True)
    value = models.CharField(max_length=500)
    frequency = models.IntegerField(default=1)
    
    objects = MultiAutocompleteCacheManager()
    
    class Meta:
        unique_together = ('key_hash', 'value')
        
    