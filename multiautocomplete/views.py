from django.utils import simplejson
from django.http import HttpResponse, HttpResponseForbidden
from multiautocomplete.models import MultiAutocompleteCache
 
def ajax_search_view(request, key):
    '''
    Returns list of string for given key and query.
    '''
    if request.method=="GET":
        if request.GET.has_key('query'):
            query = request.GET['query']
            
            query_params = {
                    'key_hash': key,
                    'value__icontains': query
            }
            if request.GET.has_key('context'):
                query_params['context'] = int(request.GET['context'])
            
            objs = MultiAutocompleteCache.objects.filter(**query_params).order_by('-frequency')[:20]
            ret_value = {'query': query, 'suggestions': [ x.value for x in objs]}
            return HttpResponse(simplejson.dumps(ret_value))
    return HttpResponseForbidden()
    

