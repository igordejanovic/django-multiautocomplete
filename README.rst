Multi Autocomplete
==================

Widget for multiple autocompletions.

Uses `autocomplete jQuery plugin`_, version 1.1.3

.. _`autocomplete jQuery plugin`: http://www.devbridge.com/projects/autocomplete/jquery/

Installation
------------

- Download_ and unpack archive or `clone source`_ from bitbucket.org and execute::

    python setup.py install

.. _Download: https://bitbucket.org/igord/django-multiautocomplete/downloads
.. _`clone source`: https://bitbucket.org/igord/django-multiautocomplete/src

- Include application in INSTALLED_APPS::

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.sites',
        'django.contrib.admin',
        'multiautocomplete',
        ...
    )

- Include urls.py in project urls file::

    urlpatterns += patterns('',
        # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
        # to INSTALLED_APPS to enable admin documentation:
        # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

        (r'^multiautocomplete/', include('multiautocomplete.urls')),
    )
    
- Declare MULTI_AUTOCOMPLETE_FIELDS in settings.py (see below for example). 

- Create db table::

    python manage.py syncdb

- Collect static files::
    
    python manage.py collectstatics


Usage
-----

Following django model is assumed in the examples::

    class Recipe(models.Model):
        
        name = models.CharField(_(u'Name'), max_length=100)
        ingredients = models.TextField(_(u'Ingredients'))
        description = models.TextField(_(u'Description'), null=True, blank=True)
        
        def __unicode__(self):
            return self.name


MultiAutoComplete fields are declared in settings.py::

    MULTI_AUTOCOMPLETE_FIELDS = [
        # (app, lowercase_model, field)
        ('simplecookbook', 'recipe', 'ingredients'),
    ]

Widget can be replaced in ModelAdmin.get_form::
    
    from multiautocomplete.widgets import MultiAutoComplete

    class RecipeAdmin(admin.ModelAdmin):

        def get_form(self, request, obj=None, **kwargs):
            form = super(RecipeAdmin, self).get_form(request, obj, **kwargs)
            
            form.base_fields['ingredients'].widget = \
                MultiAutoComplete('simplecookbook:recipe:ingredients')
                                
            return form

First parameter of MultiAutoComplete constructor is field identification in the form
"app_label:lowercase_model:field_name".
Context is optional integer. If supplied only entries for given field and given
context are presented to the user.


Cache autoupdate
~~~~~~~~~~~~~~~~

If context is used model should implement get_context method as a support for cache autoupdate::

    def get_context(self):
        return some_context

For cache autoupdate call::

  MultiAutocompleteCache.objects.update_cache(obj)

after obj is saved (e.g. in ModelAdmin.save_model)::

    def save_model(self, request, obj, form, change):
        super(RecipeAdmin, self).save_model(request, obj, form, change)
        
        MultiAutocompleteCache.objects.update_cache(obj)

For one-shot cache rebuild execute script scripts/cache_rebuild.py
This script can be called from cron.

Parser configuration
~~~~~~~~~~~~~~~~~~~~

Parsing for cache update can be configured with with MULTI_AUTOCOMPLETE_PARSER function defined in settings.py

For example, here we consider each entry to be on a different line and we take 
text up to the char "^"::

  MULTI_AUTOCOMPLETE_PARSER = lambda in_str: [ x.strip().split('^')[0] for x in in_str.splitlines() if x.strip()]

Default parser splits on newlines.


TODO
----

- Support for simple input fields (currently only textarea is supported).
- MultiAutocomplete currently has slight dependency on django admin. That 
  dependency should be removed.
- Ajax calls are not secured properly. That **must** be addressed before 
  `production ready` stage.
