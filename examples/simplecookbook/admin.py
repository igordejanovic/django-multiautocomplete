#-*- coding: utf-8 -*-
from django.contrib import admin
from multiautocomplete.widgets import MultiAutoComplete
from multiautocomplete.models import MultiAutocompleteCache
from models import *

class RecipeAdmin(admin.ModelAdmin):
    
    
    def get_form(self, request, obj=None, **kwargs):
        form = super(RecipeAdmin, self).get_form(request, obj, **kwargs)
        
        form.base_fields['ingredients'].widget = \
            MultiAutoComplete('simplecookbook:recipe:ingredients')
                              
        return form
        
    def save_model(self, request, obj, form, change):
        super(RecipeAdmin, self).save_model(request, obj, form, change)
        
        MultiAutocompleteCache.objects.update_cache(obj)
        
admin.site.register(Recipe, RecipeAdmin)
