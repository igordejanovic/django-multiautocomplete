from django.db import models
from django.utils.translation import ugettext as _

class Recipe(models.Model):
    
    name = models.CharField(_(u'Name'), max_length=100)
    ingredients = models.TextField(_(u'Ingredients'))
    description = models.TextField(_(u'Description'), null=True, blank=True)
    
    def __unicode__(self):
        return self.name
        
        
            
